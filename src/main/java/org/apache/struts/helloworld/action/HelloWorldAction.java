package org.apache.struts.helloworld.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts.helloworld.model.MessageStore;

public class HelloWorldAction extends ActionSupport {
    private static final long serialVersionUID = -1426967826890296347L;
    private MessageStore messageStore;

    @Override
    public String execute() {
        messageStore = new MessageStore();
        if (userName!=null) {
            messageStore.setMessage(messageStore.getMessage() + " " + userName);
        }
        helloCount++;
        return SUCCESS;
    }

    public MessageStore getMessageStore() {
        return messageStore;
    }

    private static int helloCount = 0;
    public int getHelloCount() {
        return helloCount;
    }

    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
